## TP Gamification

Sujet: https://docs.google.com/document/d/1lga9mCrNfFmNq7JcxLxYPfCA0Rw-EModhX47aO0pgEE/edit

Par Vincent KLEINBAUER et Axel PACCALIN.


Le rapport est intégré au script (notebook) 3 versions sont disponibles:
 - Source (.ipynb)
 - Présentation (.html, recommandé)
 - Compatibilité (.pdf)
 
Une version est aussi automatiquement hébergée par la forge à l'adresse suivante: http://p1709476.pages.univ-lyon1.fr/tp-gamification/#/
